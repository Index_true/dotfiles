#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'

for f in ~/.bash/*; do source $f; done # Source all files in .bash folder

