## XDG CONFIG DIRECTORIES
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share

export TERMINAL=lxterminal # Set terminal for i3-sensible-terminal

export EDITOR=nvim

eval $(ssh-agent) # Start ssh agent 

source $HOME/.bashrc # Make sure bashrc is sourced on TTY login shell
