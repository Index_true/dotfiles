call plug#begin('~/.local/share/nvim/plugged')

" NCM2, formerly known as nvim-completion-manager, is a slim, fast and hackable completion framework for neovim.
Plug 'ncm2/ncm2'

" Dépendance de ncm2 : This is my attempt on writing a remote plugin framework without :UpdateRemotePlugins.
Plug 'roxma/nvim-yarp'

" Support for LanguageServer Protocol ( https://langserver.org/ ) for NCM2
Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'ncm2/ncm2-vim-lsp'

" Ncm2 useful completions
Plug 'ncm2/ncm2-bufword'
Plug 'ncm2/ncm2-path'

" Graphviz support
Plug 'liuchengxu/graphviz.vim'

" This plugin adds Go language support for Vim
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

"I'm not going to lie to you; fugitive.vim may very well be the best Git wrapper of all time.
Plug 'tpope/vim-fugitive'

" emmet-vim is a vim plug-in which provides support for expanding abbreviations similar to emmet.
Plug 'mattn/emmet-vim'

" ALE (Asynchronous Lint Engine) is a plugin for providing linting (checking syntax and semantics)
" To enable python support do 'pip install 'python-language-server[all]''
Plug 'w0rp/ale'

" Javascript completion
Plug 'ncm2/ncm2-tern',  {'do': 'npm install'}

call plug#end()

"Enable mouse support
set mouse=a

"Disable wordwrapping
set nowrap

"Enable True color support
set termguicolors

"Enable dark background
set background=dark

"Enable deep-space colorscheme
colorscheme slate

" Automatic syntax fix using ALE when saving files
let g:ale_fix_on_save = 1

"ALE Fixers configuration
let g:ale_fixers = {'python': ['autopep8','black','isort','yapf'], '*': ['remove_trailing_lines','trim_whitespace']}

" Ncm2 Config
" enable ncm2 for all buffers
autocmd BufEnter * call ncm2#enable_for_buffer()

" IMPORTANT: :help Ncm2PopupOpen for more information
set completeopt=noinsert,menuone,noselect

" suppress the annoying 'match x of y', 'The only match' and 'Pattern not
" found' messages
set shortmess+=c

" When the <Enter> key is pressed while the popup menu is visible, it only
" hides the menu. Use this mapping to close the menu and also start a new
" line.
inoremap <expr> <CR> (pumvisible() ? "\<c-y>\<cr>" : "\<CR>")

" Use <TAB> to select the popup menu:
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"


"Language servers
"Python ( To install the python language server do 'pip install python-language-server' )
if executable('pyls')
    au User lsp_setup call lsp#register_server({
        \ 'name': 'pyls',
        \ 'cmd': {server_info->['pyls']},
        \ 'whitelist': ['python'],
        \ })
endif
